package dataDriverTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class excelData {

	@DataProvider
	public String[][] getdata() throws IOException {
		
       File f = new File("/home/ilaiyathasan/Documents/TestData/TestData2.xlsx");
		
		FileInputStream fis = new FileInputStream(f);
		
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		
		XSSFSheet sh = wb.getSheetAt(0);
		
		int rows = sh.getPhysicalNumberOfRows();
		int columns = sh.getRow(0).getLastCellNum();
		String[][] data = new String[rows-1][columns];
		for (int i = 0; i < rows-1; i++) {
			
			for(int j=0; j<columns;j++) {
				DataFormatter df = new DataFormatter();
				data[i][j] = df.formatCellValue(sh.getRow(i+1).getCell(j));
//				String cellvalue = sh.getRow(i).getCell(j).getStringCellValue();
//				System.out.println(cellvalue);
			}
		}
		return data;
	}

}
