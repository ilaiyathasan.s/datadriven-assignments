package dataDriverTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class fetchingValueThroughJxl {

	public static void main(String[] args) throws BiffException, IOException {
		
		File f = new File("/home/ilaiyathasan/Documents/TestData/TestData1.xls");
		
		FileInputStream fis = new FileInputStream(f);
		
		Workbook wb = Workbook.getWorkbook(fis);
		
		Sheet sh = wb.getSheet("Sheet1");
		
		String celldata = sh.getCell(1, 2).getContents();
		
		System.out.println("Values present is " +celldata);
		
		int rows = sh.getRows();
		int columns = sh.getColumns();
		
		String[][] data = new String[rows-1][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				celldata = sh.getCell(j, i).getContents();
				System.out.println(celldata);
			}
			
		}
		
		
		
	}

}
