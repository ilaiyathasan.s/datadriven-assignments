package dataDriverTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jxl.Workbook;

public class fetchingDataThroughApachePoi {

	public static void main(String[] args) throws IOException {
		
		File f = new File("/home/ilaiyathasan/Documents/TestData/TestData2.xlsx");
		
		FileInputStream fis = new FileInputStream(f);
		
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		
		XSSFSheet sh = wb.getSheetAt(0);
		
		int rows = sh.getPhysicalNumberOfRows();
		for (int i = 0; i < rows; i++) {
			int columns = sh.getRow(i).getLastCellNum();
			for(int j=0; j<columns;j++) {
				String cellvalue = sh.getRow(i).getCell(j).getStringCellValue();
				System.out.println(cellvalue);
			}
		}

	}

}
