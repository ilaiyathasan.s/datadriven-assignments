package dataDriverTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterDemoWebShopScriptUsingJxl {

	public static void main(String[] args) throws BiffException, IOException {
	
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://demowebshop.tricentis.com/");
		driver.findElement(By.linkText("Register")).click();
		
		File f = new File("/home/ilaiyathasan/Documents/TestData/TestData1.xls");
		FileInputStream fis = new FileInputStream(f);
		Workbook wb = Workbook.getWorkbook(fis);
		Sheet sh = wb.getSheet("Registerdata");
		int rows = sh.getRows();
		int columns = sh.getColumns();
		for(int i=1; i<rows; i++) {
			String firstname = sh.getCell(0, i).getContents();
			String lastname = sh.getCell(1, i).getContents();
			String email = sh.getCell(2, i).getContents();
			String password = sh.getCell(3, i).getContents();
			//String confirmpassword = sh.getCell(3, i).getContents();
			driver.findElement(By.linkText("Register")).click();
			
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			driver.findElement(By.id("register-button")).click();
			
			driver.findElement(By.linkText("Log out")).click();
		}

	}

}
